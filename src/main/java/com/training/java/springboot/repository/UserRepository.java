package com.training.java.springboot.repository;

import com.training.java.springboot.entity.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, String> {
}
