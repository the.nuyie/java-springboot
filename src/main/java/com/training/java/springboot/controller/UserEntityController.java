package com.training.java.springboot.controller;

import com.training.java.springboot.entity.User;
import com.training.java.springboot.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/userentities")
public class UserEntityController {

    private Logger logger = LogManager.getLogger(UserEntityController.class);

    @Autowired
    private UserService userService;

    @GetMapping
    public Iterable<User> getAllUsers() {
        Iterable<User> users = userService.getAllUsers();
        logger.info("Existing users : {}", users);
        return users;
    }
    @PostMapping
    public User createUser(@RequestBody User user) {
        logger.info("User want to be created : {}", user);
        userService.createUser(user);
        logger.info("User {} added.", user.getName());
        // Return current user that just created
        return user;
    }
    @DeleteMapping("/{user}")
    public String deleteUser(@PathVariable String user) {
        // System.out.println("List of existing users : "+users);
        // System.out.println("User want to be deleted : "+user);

        Iterable<User> users = userService.getAllUsers();

        logger.info("User want to be deleted : {}",user);
        logger.debug("List of existing users : {}",users);

        String strReturn = null;

        if(users != null){
            for (User user1 : users) {

                if(user1.getName().equals(user)){
                    // Found user.
                    userService.deleteUser(user1.getId());
                    strReturn = "User deleted: " + user;
                    break;
                }
            }
        }else{
            strReturn = "User not found";
        }
        return strReturn;
    }
}
