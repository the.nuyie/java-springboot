package com.training.java.springboot.controller;

import com.training.java.springboot.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/userbeans")
public class UserBeanController {

    private Logger logger = LogManager.getLogger(UserBeanController.class);

    private List<User> users = new ArrayList<User>();
    @GetMapping
    public List<User> getAllUsers() {
        logger.info("Existing users : {}", users);
        return users;
    }
    @PostMapping
    public User createUser(@RequestBody User user) {
        logger.info("User want to be created : {}", users);
        users.add(user);
        logger.info("User {} added.", user.getName());
        // Return current user that just created
        return user;
    }
    @DeleteMapping("/{user}")
    public String deleteUser(@PathVariable String user) {
        // System.out.println("List of existing users : "+users);
        // System.out.println("User want to be deleted : "+user);

        logger.info("User want to be deleted : {}",user);
        logger.debug("List of existing users : {}",users);

        String strReturn = null;

        if(users != null && users.size() > 0){
            for(int i=0; i<=users.size(); i++){
                User u = users.get(i);
                if(u.getName().equals(user)){
                    users.remove(u);
                    strReturn = "User deleted: " + user;
                    break;
                }
            }
        }else{
            strReturn = "User not found";
        }
        return strReturn;
    }
}
